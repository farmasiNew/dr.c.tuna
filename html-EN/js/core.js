var menuOpen = function () {
  $("#menu").addClass("active")
},
  menuClose = function () {
    $("#menu").removeClass("active")
  };
$("a.scroll").bind("click", function (e) {
  menuClose();
});
$("#lang-wrapper .active").bind("click", function (e) {
  e.preventDefault()
});

//
$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(window).on('resize scroll', function() {
  $('section').each(function() {
    if ($(this).isInViewport()) {
      setTimeout(() => {
        $(this).find('.reveal-on-scroll').addClass('animate__animated ' + $(this).find('.reveal-on-scroll').attr('data-animation'));
      }, 500);
     }// else {
    //   $(this).find('.reveal-on-scroll').removeClass('animate-fade-in');
    // }
  });
});