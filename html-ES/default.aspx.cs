﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LandingPage_Default : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (Session["WebDisplayName"] != null && Request.RequestContext.RouteData.Values["webDisplayName"] == null)
    {
      Response.Redirect("/" + Session["WebDisplayName"] + "/vfxprofondoten");
    }
    else
    {
      Session["WebDisplayName"] = Request.RequestContext.RouteData.Values["webDisplayName"];
    }
  }
}